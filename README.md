# cascrel

An implementation of the Cascade Correlation neural network architecture.
Features Python bindings for ease of use out-of-the-box.

### Prerequisites

You need to have the following software/libraries installed:

|Name    |Description    |Minimum version|
|--------|:-------------:|:-------------:|
|cmake   |Building       |3.10           |
|gcc     |C++ compiler   |7.x            |
|spdlog  |Logging        |1.4.x          |
|Eigen   |Linear algebra |3.x            |
|catch   |Unit tests     |2.x            |

Optionally, these automatically detected
dependencies improve the general experience:

* ccache - speeds up building time
* OpenMP support in gcc - introduces multiprocessing
in certain algebra operations

### Building

#### Step 1: Configure cmake

Run from project root:

* `mkdir cmake-build` (all cmake-build* dirs are ignored by git)
* `cd cmake-build`
* `cmake -DCMAKE_BUILD_TYPE=<build-type> ..`

where `<build-type>` is one of {`Debug`, `Release`}.

You can also set the `CMAKE_INSTALL_PREFIX` variable with
`-DCMAKE_INSTALL_PREFIX=<path>` to specify where the built
libs and headers shall be installed, e.g. `/usr/local`.

For example, if you want to install files in `/usr/local`:

`cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local ..`

#### Step 2: Build a target

`make <target name>`

Special targets:

* `all` - build all compilable targets (except for tests)
* `clean` - clean all build files
* `help` - list available targets
* `install` - build and install (or update) all targets (C++ headers and libs)
* `check` - builds and runs tests

### Installing

#### Step 1: Install the cascrel library

Use `make install` to install the cascrel library (according to the settings above).

### Components (targets)

#### code (target `cascrel`)

This is the library containing the C++ API
for creating, training and evaluating Cascade Correlation networks.

Installs lib in `<install-prefix>/lib`
and headers in `<install-prefix>/include/cascrel`.

#### test (target `cascrel_test`)

Here you can build library tests.
