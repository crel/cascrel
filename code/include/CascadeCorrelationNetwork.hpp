#ifndef CASCREL_CASCADECORRELATIONNETWORK_HPP
#define CASCREL_CASCADECORRELATIONNETWORK_HPP

#include <memory>
#include <vector>

#include <spdlog/logger.h>

#include "common.hpp"

#include "activation/ActivationFunction.hpp"
#include "History.hpp"
#include "initializers/Initializer.hpp"
#include "layers/HiddenLayer.hpp"
#include "layers/OutputLayer.hpp"
#include "loss/LossFunction.hpp"
#include "optimizers/Optimizer.hpp"
#include "utility/BatchRange.hpp"

namespace cascrel {

static const unsigned int DEFAULT_PATIENCE = 500;
static const Scalar DEFAULT_TOLERANCE = 0.000001;
static const unsigned int DEFAULT_MAX_HIDDEN_LAYERS = 20;
static const Scalar DEFAULT_MAX_LOSS = 0.1;

class CascadeCorrelationNetwork {
public:
    /// This constructor is generally not intended for outside use.
    /// In most cases you will want to use
    /// the \ref cascrel::factory::Builder "Builder" class.
    /// Warning: the \ref cascrel::initializers::Initializer "Initializer",
    /// \ref cascrel::optimizers::Optimizer "Optimizer",
    /// \ref cascrel::activation::ActivationFunction "ActivationFunction"
    /// and \ref cascrel::loss::LossFunction "LossFunction" objects
    /// must be created with the new operator.
    CascadeCorrelationNetwork(SizeType inDim, SizeType outDim,
                              initializers::Initializer& hiddenInitializer,
                              initializers::Initializer& outputInitializer,
                              optimizers::Optimizer& hiddenOptimizer,
                              optimizers::Optimizer& outputOptimizer,
                              activation::ActivationFunction& hiddenActivation,
                              activation::ActivationFunction& outputActivation,
                              loss::LossFunction& lossFunction);

    /// Train the network on the given training set
    /// \param x matrix of training input samples
    /// \param y matrix of "label" data (target output values)
    /// \param batchSize size of training batches
    /// \param patience maximum epochs to continue training within margin of tolerance
    /// \param tolerance maximum tolerable change in optimized values
    /// \param maxHiddenLayers maximum allowed number of hidden layers
    /// \param maxLoss maximum allowed loss value
    /// \return a cascrel::History object with records from this training session
    History train(const RowMatrix &x, const RowMatrix &y,
                  SizeType batchSize = utility::DEFAULT_BATCH_SIZE,
                  unsigned int patience = DEFAULT_PATIENCE,
                  Scalar tolerance = DEFAULT_TOLERANCE,
                  unsigned int maxHiddenLayers = DEFAULT_MAX_HIDDEN_LAYERS,
                  Scalar maxLoss = DEFAULT_MAX_LOSS,
                  unsigned int safetyEpochLimit = 0);

    /// Measure network performance on the given data
    /// \param x matrix of evaluation input samples
    /// \param y matrix of evaluation "label" data (target output values)
    RowVector evaluate(const RowMatrix& x, const RowMatrix& y) const;

    /// Compute predictions for the given input
    /// \param x row matrix of input samples
    /// \return a row matrix of predicted output
    RowMatrix predict(const RowMatrix& x) const;

    /// Return current number of hidden layers
    /// \return number of hidden layers connected to the network
    SizeType getNumHidden() const;

    /// Set logging level
    /// \param logLevel log output level
    void setLogLevel(LogLevel logLevel);

private:
    void checkInputAssertions(const RowMatrix& x) const;

    void checkOutputAssertions(const RowMatrix& y) const;

    static void checkSampleCounts(const RowMatrix& x, const RowMatrix& y);

    RowMatrix calculateHiddenOutputs(const RowMatrix& x) const;

    Scalar trainOutputLayer(const RowMatrix& x, const RowMatrix& y);

    Scalar trainNewHiddenLayer(const RowMatrix& x, const RowMatrix& y,
                               layers::HiddenLayer& candidate);

    Scalar trainOutputOnBatches(const RowMatrix& x, const RowMatrix& y, SizeType batchSize);

    Scalar trainNewHiddenOnBatches(const RowMatrix& x, const RowMatrix& y,
                                   layers::HiddenLayer& candidate, SizeType batchSize);

    Scalar trainOutputWithPatience(const RowMatrix& x, const RowMatrix& y,
                                   SizeType batchSize,
                                   History& history,
                                   unsigned int patience, Scalar tolerance,
                                   unsigned int safetyEpochLimit);

    void trainNewHiddenWithPatience(const RowMatrix& x, const RowMatrix& y,
                                    SizeType batchSize,
                                    History& history,
                                    layers::HiddenLayer& candidate,
                                    unsigned int patience, Scalar tolerance,
                                    unsigned int safetyEpochLimit);

    static RowMatrix addBias(const RowMatrix& x);

private:
    std::unique_ptr<initializers::Initializer> mHiddenInitializer;
    std::unique_ptr<initializers::Initializer> mOutputInitializer;
    std::unique_ptr<optimizers::Optimizer> mHiddenOptimizer;
    std::unique_ptr<optimizers::Optimizer> mOutputOptimizer;

    std::vector<layers::HiddenLayer> mHiddenLayers;
    layers::OutputLayer mOutputLayer;

    std::unique_ptr<activation::ActivationFunction> mHiddenActivation;
    std::unique_ptr<activation::ActivationFunction> mOutputActivation;
    std::unique_ptr<loss::LossFunction> mLossFunction;

    const SizeType mInputDimension;
    const SizeType mOutputDimension;

    unsigned int mTotalEpochs;

    std::shared_ptr<spdlog::logger> mLogger;
};

} // namespace cascrel

#endif //CASCREL_CASCADECORRELATIONNETWORK_HPP
