#ifndef CASCREL_CONTINUOUSFUNCTION_HPP
#define CASCREL_CONTINUOUSFUNCTION_HPP

#include "common.hpp"

namespace cascrel {

class ContinuousFunction {
public:
    /// Computes the function value at x
    /// \param x function argument
    /// \return value of f(x)
    virtual Scalar operator()(Scalar x) const = 0;

    /// Computes the derivative of function at x
    /// \param x derivative argument
    /// \return value of f'(x)
    virtual Scalar derivative(Scalar x) const = 0;
};

} // namespace cascrel

#endif //CASCREL_CONTINUOUSFUNCTION_HPP
