#ifndef CASCREL_ACTIVATIONFUNCTION_HPP
#define CASCREL_ACTIVATIONFUNCTION_HPP

#include "Cloneable.hpp"
#include "ContinuousFunction.hpp"

namespace cascrel {
namespace activation {

class ActivationFunction : public Cloneable, public ContinuousFunction {
public:
    virtual ~ActivationFunction() = default;
};

} // namespace activation
} // namespace cascrel

#endif //CASCREL_ACTIVATIONFUNCTION_HPP
