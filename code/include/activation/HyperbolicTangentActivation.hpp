#ifndef CASCREL_HYPERBOLICTANGENTACTIVATION_HPP
#define CASCREL_HYPERBOLICTANGENTACTIVATION_HPP

#include "common.hpp"

#include "activation/ActivationFunction.hpp"

namespace cascrel {
namespace activation {

class HyperbolicTangentActivation : public ActivationFunction {
public:
    Scalar operator()(Scalar x) const override;

    Scalar derivative(Scalar x) const override;

    ActivationFunction* clone() const override;
};

} // namespace activation
} // namespace cascrel

#endif //CASCREL_HYPERBOLICTANGENTACTIVATION_HPP
