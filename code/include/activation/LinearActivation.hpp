#ifndef CASCREL_LINEARACTIVATION_HPP
#define CASCREL_LINEARACTIVATION_HPP

#include "common.hpp"

#include "activation/ActivationFunction.hpp"

namespace cascrel {
namespace activation {

class LinearActivation : public ActivationFunction {
public:
    Scalar operator()(Scalar x) const override;

    Scalar derivative(Scalar x) const override;

    ActivationFunction* clone() const override;
};

} // namespace activation
} // namespace cascrel

#endif //CASCREL_LINEARACTIVATION_HPP
