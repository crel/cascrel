#ifndef CASCREL_CONSTRUCT_HPP
#define CASCREL_CONSTRUCT_HPP

#include <type_traits>
#include <utility>

namespace cascrel {
namespace factory {

template <typename Base>
class Construct {
public:
    template <typename Concrete, typename... Args>
    static Base* as(Args&& ... args);
};

///
/// \tparam Base base type (e.g. interface)
/// \tparam Concrete concrete type derived from Base
/// \tparam Args
/// \param args Concrete constructor arguments
/// \return pointer to newly constructed Concrete instance
template <typename Base>
template <typename Concrete, typename... Args>
Base* Construct<Base>::as(Args&& ... args) {
    static_assert(std::is_base_of<Base, Concrete>::value,
            "Construct can only generate types derived from Base type");
    static_assert(std::is_constructible<Concrete, Args...>::value,
            "Construct cannot construct Concrete type from given Args");

    return new Concrete(std::forward<Args>(args)...);
}

} // namespace factory
} // namespace cascrel

#endif //CASCREL_CONSTRUCT_HPP
