#ifndef CASCREL_RANDOMINITIALIZER_HPP
#define CASCREL_RANDOMINITIALIZER_HPP

#include "common.hpp"

#include "Cloneable.hpp"

namespace cascrel {
namespace initializers {

class Initializer : public Cloneable {
public:
    virtual Scalar operator()() = 0;

    virtual ~Initializer() = default;
};

} // namespace initializers
} // namespace cascrel

#endif //CASCREL_RANDOMINITIALIZER_HPP
