#ifndef CASCREL_NORMALRANDOMINITIALIZER_HPP
#define CASCREL_NORMALRANDOMINITIALIZER_HPP

#include <random>

#include "common.hpp"

#include "initializers/Initializer.hpp"

namespace cascrel {
namespace initializers {

class NormalInitializer : public Initializer {
public:
    explicit NormalInitializer(Scalar mean = 0., Scalar standardDeviation = 1.);

    explicit NormalInitializer(unsigned seed,
                               Scalar mean = 0., Scalar standardDeviation = 1.);

    Scalar operator()() override;

    Initializer* clone() const override;

private:
    std::default_random_engine mRandomEngine;
    std::normal_distribution<Scalar> mNormalDistribution;
};

} // namespace initializers
} // namespace cascrel

#endif //CASCREL_NORMALRANDOMINITIALIZER_HPP
