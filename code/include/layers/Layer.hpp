#ifndef CASCREL_LAYER_HPP
#define CASCREL_LAYER_HPP

#include "common.hpp"

#include "initializers/Initializer.hpp"

namespace cascrel {
namespace layers {

class Layer {
public:
    /// Get input size
    /// \return input size
    virtual SizeType getInputDimension() const = 0;

    /// Get output size
    /// \return output size
    virtual SizeType getOutputDimension() const = 0;

    /// Compute output for given input
    /// \param input a row matrix representing input to the layer
    /// \return a matrix representing output of the layer
    virtual RowMatrix calculateOutput(const RowMatrix& input) const = 0;

    /// Get a view of the weights in the layer
    /// to inspect/edit the layer's weights
    /// \return a view of the layer's weight matrix
    virtual MatrixView weights() = 0;

    /// Enlarge the layer weight matrix by n rows (if supported)
    /// \param n number of rows to add to the matrix
    virtual void addRows(SizeType n) = 0;
};

} // namespace layers
} // namespace cascrel

#endif //CASCREL_LAYER_HPP
