#ifndef CASCREL_OUTPUTLAYER_HPP
#define CASCREL_OUTPUTLAYER_HPP

#include "common.hpp"

#include "initializers/Initializer.hpp"
#include "layers/Layer.hpp"

namespace cascrel {
namespace layers {

class OutputLayer : public Layer {
public:
    OutputLayer(SizeType inDim, SizeType outDim,
                initializers::Initializer& initializer);

    explicit OutputLayer(const Matrix& weights);

    SizeType getInputDimension() const override;

    SizeType getOutputDimension() const override;

    RowMatrix calculateOutput(const RowMatrix& input) const override;

    MatrixView weights() override;

    void addRows(SizeType n) override;

private:
    Matrix mWeights;
};

} // namespace layers
} // namespace cascrel

#endif //CASCREL_OUTPUTLAYER_HPP
