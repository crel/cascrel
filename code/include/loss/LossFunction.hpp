#ifndef CASCREL_LOSSFUNCTION_HPP
#define CASCREL_LOSSFUNCTION_HPP

#include "Cloneable.hpp"
#include "ContinuousFunction.hpp"

namespace cascrel {
namespace loss {

class LossFunction : public Cloneable, public ContinuousFunction {
public:
    virtual ~LossFunction() = default;
};

} // namespace loss
} // namespace cascrel

#endif //CASCREL_LOSSFUNCTION_HPP
