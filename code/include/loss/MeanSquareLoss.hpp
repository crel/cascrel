#ifndef CASCREL_MEANSQUARELOSS_HPP
#define CASCREL_MEANSQUARELOSS_HPP

#include "loss/LossFunction.hpp"

namespace cascrel {
namespace loss {

class MeanSquareLoss : public LossFunction {
public:
    Scalar operator()(Scalar x) const override;

    Scalar derivative(Scalar x) const override;

    LossFunction* clone() const override;
};

} // namespace loss
} // namespace cascrel

#endif //CASCREL_MEANSQUARELOSS_HPP
