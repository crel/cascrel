#ifndef CASCREL_BATCHPTR_HPP
#define CASCREL_BATCHPTR_HPP

#include "common.hpp"

namespace cascrel {
namespace utility {

class BatchPtr {
public:
    explicit BatchPtr(Eigen::Block<const RowMatrix> wrapped);

    const Eigen::Block<const RowMatrix>* operator->() const;

private:
    Eigen::Block<const RowMatrix> mWrapped;
};

} // namespace utility
} // namespace cascrel

#endif //CASCREL_BATCHPTR_HPP
