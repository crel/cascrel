#ifndef CASCREL_BATCHRANGE_HPP
#define CASCREL_BATCHRANGE_HPP

#include "common.hpp"
#include "utility/BatchPtr.hpp"

namespace cascrel {
namespace utility {

static const SizeType DEFAULT_BATCH_SIZE = 32;

class BatchRange {
public:
    class iterator {
    public:
        using iterator_category = std::input_iterator_tag;
        using value_type = Eigen::Block<const RowMatrix>;
        using pointer = BatchPtr;
        using reference = Eigen::Block<const RowMatrix>;

    public:
        iterator(const BatchRange& range, SizeType position);

        iterator& operator++();
        iterator operator++(int);

        bool operator==(const iterator& other) const;
        bool operator!=(const iterator& other) const;

        reference operator*() const;
        pointer operator->() const;

    private:
        reference getBatch() const;

    private:
        const BatchRange& mRange;
        SizeType mPosition;
    };

    explicit BatchRange(const RowMatrix& matrix, SizeType batchSize = DEFAULT_BATCH_SIZE);

    iterator begin() const;
    iterator end() const;

    SizeType getBatchNumber() const;

private:
    const RowMatrix& mMatrix;
    const SizeType batchSize;
};

} // namespace utility
} // namespace cascrel

#endif //CASCREL_BATCHRANGE_HPP
