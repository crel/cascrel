#include "utility/BatchPtr.hpp"

using namespace cascrel::utility;

using cascrel::RowMatrix;

BatchPtr::BatchPtr(Eigen::Block<const RowMatrix> wrapped) : mWrapped(wrapped) {
}

const Eigen::Block<const RowMatrix>* BatchPtr::operator->() const {
    return &mWrapped;
}
