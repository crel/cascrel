#include "optimizers/DeltaRuleOptimizer.hpp"

using namespace cascrel;
using namespace cascrel::optimizers;

DeltaRuleOptimizer::DeltaRuleOptimizer(Scalar learningRate)
        : mLearningRate(learningRate) {
}

void DeltaRuleOptimizer::optimize(const RowMatrix& derivatives, const RowMatrix& x,
        MatrixView& optimizedWeights) {
    optimizedWeights += x.transpose()
            * derivatives
            * mLearningRate;
}

Optimizer* DeltaRuleOptimizer::clone() const {
    return new DeltaRuleOptimizer(*this);
}
