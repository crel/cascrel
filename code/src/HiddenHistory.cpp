#include "HiddenHistory.hpp"

using namespace cascrel;

const std::vector<Scalar>& HiddenHistory::getCovarianceRecords() const {
    return mCovarianceRecords;
}

void HiddenHistory::addNewCovarianceRecord(Scalar record) {
    mCovarianceRecords.push_back(record);
}
