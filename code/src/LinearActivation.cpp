#include "activation/LinearActivation.hpp"

using namespace cascrel;
using namespace cascrel::activation;

static const Scalar LINEAR_DERIVATIVE = 1.;

Scalar LinearActivation::operator()(Scalar x) const {
    return x;
}

Scalar LinearActivation::derivative(Scalar) const {
    return LINEAR_DERIVATIVE;
}

ActivationFunction* LinearActivation::clone() const {
    return new LinearActivation(*this);
}
