#include "layers/OutputLayer.hpp"

#include <functional>

using namespace cascrel;
using namespace cascrel::layers;

using cascrel::initializers::Initializer;

OutputLayer::OutputLayer(SizeType inDim, SizeType outDim, Initializer& initializer)
        : mWeights(Matrix::NullaryExpr(inDim, outDim, std::ref(initializer))) {
}

OutputLayer::OutputLayer(const Matrix& weights)
        : mWeights(weights) {
}

SizeType OutputLayer::getInputDimension() const {
    return mWeights.rows();
}

SizeType OutputLayer::getOutputDimension() const {
    return mWeights.cols();
}

RowMatrix OutputLayer::calculateOutput(const RowMatrix& input) const {
    return input * mWeights;
}

MatrixView OutputLayer::weights() {
    return MatrixView(mWeights.data(), mWeights.rows(), mWeights.cols());
}

void OutputLayer::addRows(SizeType n) {
    mWeights.conservativeResize(mWeights.rows() + n, Eigen::NoChange);
}
