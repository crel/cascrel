#include "activation/SigmoidActivation.hpp"

#include <cmath>

using namespace cascrel;
using namespace cascrel::activation;

Scalar SigmoidActivation::operator()(Scalar x) const {
    return 1. / (1. + std::exp(-x));
}

Scalar SigmoidActivation::derivative(Scalar x) const {
    const Scalar sigmoid = operator()(x);

    return sigmoid * (1. - sigmoid);
}

ActivationFunction* SigmoidActivation::clone() const {
    return new SigmoidActivation(*this);
}
