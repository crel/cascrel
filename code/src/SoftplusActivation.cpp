#include "activation/SoftplusActivation.hpp"

#include <cmath>

using namespace cascrel;
using namespace cascrel::activation;

Scalar SoftplusActivation::operator()(Scalar x) const {
    return std::log(1. + std::exp(x));
}

Scalar SoftplusActivation::derivative(Scalar x) const {
    return 1. / (1. + std::exp(-x));
}

ActivationFunction* SoftplusActivation::clone() const {
    return new SoftplusActivation(*this);
}
