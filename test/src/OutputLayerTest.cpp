#include "layers/OutputLayer.hpp"
#include "CatchTestConvenience.hpp"

#include <memory>

#include "factory/Construct.hpp"
#include "initializers/NormalInitializer.hpp"
#include "initializers/Initializer.hpp"

static const cascrel::SizeType INPUT_DIMENSION = 3;
static const cascrel::SizeType OUTPUT_DIMENSION = 4;

using cascrel::factory::Construct;
using cascrel::initializers::NormalInitializer;
using cascrel::initializers::Initializer;
using cascrel::layers::OutputLayer;

class OutputLayerTest {
public:
    OutputLayerTest()
            : initializer(
            Construct<Initializer>::as<NormalInitializer>()),
              outputLayer(INPUT_DIMENSION, OUTPUT_DIMENSION,
                          *initializer) {
        setLayerWeights();
    }

protected:
    std::unique_ptr<Initializer> initializer;
    OutputLayer outputLayer;

private:
    void setLayerWeights() {
        cascrel::Matrix weights{INPUT_DIMENSION, OUTPUT_DIMENSION};
        weights <<
                4, 7, 10, 13,
                5, 8, 11, 14,
                6, 9, 12, 15;

        outputLayer = OutputLayer(weights);
    }
};

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest, ShouldReturnInputDimension) {
    REQUIRE_EQ(INPUT_DIMENSION, outputLayer.getInputDimension());
}

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest, ShouldReturnOutputDimension) {
    REQUIRE_EQ(OUTPUT_DIMENSION, outputLayer.getOutputDimension());
}

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest, ShouldCalculateOutputCorrectly) {
    cascrel::RowMatrix v{1, INPUT_DIMENSION};
    v << 1, 2, 3;

    cascrel::RowMatrix expectedOutput{1, OUTPUT_DIMENSION};
    expectedOutput <<
            4 * 1 + 5 * 2 + 6 * 3,
            7 * 1 + 8 * 2 + 9 * 3,
            10 * 1 + 11 * 2 + 12 * 3,
            13 * 1 + 14 * 2 + 15 * 3;

    REQUIRE_EQ(expectedOutput, outputLayer.calculateOutput(v));
}

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest, ShouldReturnCorrectMatrixView) {
    cascrel::Matrix expectedWeights{INPUT_DIMENSION, OUTPUT_DIMENSION};
    expectedWeights <<
            4, 7, 10, 13,
            5, 8, 11, 14,
            6, 9, 12, 15;

    REQUIRE_EQ(expectedWeights, outputLayer.weights());
}

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest,
                          ShouldReturnModifiedWeightsWhenModified) {
    cascrel::Matrix expectedWeights{INPUT_DIMENSION, OUTPUT_DIMENSION};
    expectedWeights <<
            4, 7, 10, 13,
            5, 8, -1, 14,
            6, 9, 12, 15;

    const cascrel::SizeType rowNumber = 1;
    const cascrel::SizeType colNumber = 2;
    const cascrel::Scalar newValue = -1;
    outputLayer.weights()(rowNumber, colNumber) = newValue;

    REQUIRE_EQ(expectedWeights, outputLayer.weights());
}

TC_METHOD_WITH_CLASS_NAME(OutputLayerTest, ShouldChangeSizeWhenResized) {
    const cascrel::SizeType rowSizeIncrease = 5;
    const cascrel::SizeType expectedRowSize = outputLayer.getInputDimension()
            + rowSizeIncrease;

    outputLayer.addRows(rowSizeIncrease);

    REQUIRE_EQ(expectedRowSize, outputLayer.getInputDimension());
}
